if (( $EUID != 0 )); then
    echo "Please run as root"
    exit 0
fi

NODE_PATH=lib
export NODE_PATH

NAME=bnode
SOURCE_DIR=/home/chakachat/repos/bappnode
SOURCE_FILE=server.js

#user=ubuntu
mkdir -p $SOURCE_DIR/logs/forever
pidfile=$SOURCE_DIR/logs/$NAME.pid
logfile=$SOURCE_DIR/logs/$NAME.log
forever_dir=$SOURCE_DIR/logs/forever

forever=forever

  echo "Starting $NAME node instance: "

  if [ ! -f $logfile ]; then
    # Create the log and pid files, making sure that
    # the target use has access to them
    touch $logfile
    #chown $user $logfile
  fi

  if [ ! -f $pidfile ]; then
    touch $pidfile
    #chown $user $pidfile
  fi

  # Launch the application
      $forever start -p $forever_dir --pidFile $pidfile -l $logfile \
      -a -d $SOURCE_DIR/$SOURCE_FILE
    RETVAL=$?

exit $RETVAL
