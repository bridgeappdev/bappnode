if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi

NAME=bnode
SOURCE_DIR=/home/chakachat/repos/bappnode
SOURCE_FILE=server.js
 
#user=ubuntu
pidfile=$SOURCE_DIR/logs/$NAME.pid
logfile=$SOURCE_DIR/logs/$NAME.log
forever_dir=$SOURCE_DIR/logs/forever

forever=forever
 
  echo -n "Shutting down $NAME node instance : "
#   We should build a script to prepare the server for shutdown.
#    $node $SOURCE_DIR/prepareForStop.js
    $forever stop $SOURCE_DIR/$SOURCE_FILE
  RETVAL=$?

exit $RETVAL
