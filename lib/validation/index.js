'use strict';

const REQUESTEXCEPTIONS = require('./request_exceptions');
const ERRORS = require('error');

const validateRequest = function(reqExceptions, request) {

	for (let i = 0; i < reqExceptions.length; i++) {
		if (typeof reqExceptions[i] == 'string') {
			reqExceptions[i] = REQUESTEXCEPTIONS[reqExceptions[i]];
		}
		const condition = eval(reqExceptions[i].condition); //jshint ignore:line
		if (reqExceptions[i].type && condition) {
			const err = new ERRORS[reqExceptions[i].type](reqExceptions[i].message,
				reqExceptions[i].code);
			throw err;
		} else if (condition) {
			throw new Error(reqExceptions[i].message);
		}
	}
	return true;
};

module.exports = {

	validateRequest: validateRequest,

	REQUESTEXCEPTIONS: REQUESTEXCEPTIONS
	
};
