'use strict';

const ERROR = require('error');

const Validator = require('jsonschema').Validator;

const validateSchema = function(data, schema) {

	const validator = new Validator();

	const validationResult = validator.validate(data, schema);

	if (validationResult.errors.length > 0) {
		throw new ERROR.JSONValidationError(
			"Error validating parameters",
			data,
			validationResult.errors
		);
		throw error;
	}
};

const parseErrorArray = function(errorDetails){
	if(
		!(Array.isArray(errorDetails) &&
		errorDetails.length > 0)
	){
		throw new TypeError("parseErrorArray parameter should be an array");
	}

	const errorMessageArray = [];
	errorDetails.forEach(function(errorObj){
		errorMessageArray.push({
			instance: errorObj.instance?errorObj.instance:errorObj.property,
			argument: errorObj.argument,
			message: errorObj.message
		});
	})

	return errorMessageArray;

};

module.exports = {
	validateSchema: validateSchema,
	parseErrorArray: parseErrorArray
}
