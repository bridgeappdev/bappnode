module.exports = {
	"properties" :{
		"bridgeId"	:{
			"type": 'string'
		},
		"whoId" :{
			"type": 'string'
		},
		"who" :{
			"type": 'string'
		},
		"pinnedId" :{
			"type": 'string'
		}
	},
	"required": [
		"bridgeId",
		"whoId",
		"who",
		"pinnedId"
	]
};
