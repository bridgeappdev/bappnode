'use strict';

const firebase = require('firebase');

const _isValid = function _isValid( notificationObject ){
	return true;
}
const notification = exports = module.exports = {};

notification.saveNotification = function saveNotification(
	userIds,
	notificationObject,
	next
){
	if( ! _isValid( notificationObject ) ){
		const err = new TypeError( "Invalid notification object" );
		LOGGER.error( err );
		if( typeof next === 'function'){
			return next( err )
		} else {
			return void 0;
		}
	}
	const notificationRef = firebase.database().ref( 'notifications' );

	if( userIds.forEach ){
		userIds.forEach( function( userId ){
			notificationRef.child( userId ).push( notificationObject, function( err ){
				if( err ) { LOGGER.error( err ); }
			} );
		});

		if( typeof next === 'function'){
			return next( null )
		} else {
			return void 0;
		}

	} else { //is a string
		notificationRef.child( userId ).push( notificationObject, function( err ){
			if( err ) { LOGGER.error( err ); }
		} );

		if( typeof next === 'function'){
			return next( null )
		} else {
			return void 0;
		}
	}

}
