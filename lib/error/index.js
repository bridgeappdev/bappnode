/* lib/error/index.js */
'use strict';

const error = module.exports = {};

const Logger = require('../logger');

const LOGGER = new Logger(process.env.NODE_ENV);

error.codes = require('./codes');

error.ErrorBase = class ErrorBase /*{Abstract}*/ {
	/**
	 * @param {string}  message
	 * @param (integer) httpStatus
	 */
	constructor(message, httpStatus) {
		this.message = message;
		this.httpStatus = httpStatus;
	}

	log(){
		LOGGER.error(this);
	}
}

// 400 Client Error

/** ErrorParams
 * @param {string}  message
 * @param {number}   httpStatus
 */

error.BadRequestError = class BadRequestError extends error.ErrorBase {
	/** #ErrorParams */
	constructor( message, httpStatus ) {
		super(message, httpStatus || error.codes.BAD_REQUEST)
	}
};

error.NotFoundError = class NotFoundError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, httpStatus) {
		super(message, httpStatus || error.codes.RESOURCE_NOT_FOUND)
	}
}

error.HTTPHeaderError = class HTTPHeaderError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, httpStatus) {
		super(message, httpStatus || error.codes.HEADER_ERROR);
	}
}

error.LoginError = class LoginError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, httpStatus) {
		super(message, httpStatus || error.codes.LOGIN_ERROR);
	}
};

error.SignUpError = class SignUpError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, httpStatus) {
		super(message, httpStatus || error.codes.SIGNUP_ERROR);
	}
};

error.ForbiddenError = class ForbiddenError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, httpStatus) {
		super(message, httpStatus || error.codes.FORBIDDEN);
	}
};

error.ConflictError = class ConflictError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, httpStatus) {
		super(message, httpStatus || error.codes.CONFLICT);
	}
};

error.JSONValidationError = class JSONValidationError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, origval, errorDetail, httpStatus) {
		super(message, httpStatus || error.codes.INVALID_JSON);
		this.errorDetail = errorDetail;
		this.origValue = origval;
	}
};

// 500 Server Error

error.RequiredParameterError = class RequiredParameterError extends error.ErrorBase {
	/** #ErrorParams */
	constructor(message, httpStatus) {
		super(message, httpStatus || error.codes.REQ_PARAM);
		//Error.captureStackTrace(this, arguments.callee);
	}
};

error.StackErrorBase = class StackErrorBase extends error.ErrorBase {
	/** { Abstract }
	 * @param {Error}
	 * @param {string} message
	 */
	constructor(errobj, message, httpStatus) {
		super(message, httpStatus);
		this.originalrror = errobj;
		Error.captureStackTrace (this);
	}
};


/** StackErrorParams
 * @param {Error}  errobj
 * @param {string} message
 * @param {number}  code
 */

error.ReqBodyParseError = class ReqBodyParseError extends error.StackErrorBase {
	/** #StackErrorParams */
	constructor(errobj, message, httpStatus) {
		super(errobj, message, httpStatus || error.codes.BAD_REQUEST);
	}
};

error.ServerError = class ServerError extends error.StackErrorBase {
	/** #StackErrorParams */
	constructor(errobj, message, httpStatus) {
		super(errobj, message, httpStatus || error.codes.SERVER_ERROR);
	}
};

error.DataBaseError = class DataBaseError extends error.StackErrorBase {
	constructor(errobj, message, httpStatus) {
		super(errobj, message, httpStatus || error.codes.DB_SERVER_ERROR);
	}
};
