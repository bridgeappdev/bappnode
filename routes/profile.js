/* routes/profile.router.js
 * Router defintion for /profile endpoint
 *
 * */
const controller = require('../controllers/profile');

const express = require('express');

const router = express.Router();

router.post( '/checkUsername/', controller.checkUsername );

router.post( '/addUsername/', controller.addUsername );

module.exports = router;
