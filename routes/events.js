/*
* routes/events.js
* Router definition for the events endpoints
*/
var express = require('express');
var controller = require('../controllers/events');
//cors = require('cors');
const router = express.Router();

// middleware specific to this router
router.use(function timeLog(req, res, next) {
	'use strict';

  console.log('Events: Time: ', Date.now());
  next();
});

//var corsOptions = require('../lib/utils.js').corsOptions;

router.post('/meetup/refreshtoken', controller.refreshToken);

module.exports = router;
