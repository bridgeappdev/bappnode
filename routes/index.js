'use strict';

module.exports = function registerRouters( app ) {

	app.use('/bridges', require('./bridges'));

	app.use('/notification', require('./notifications'));

	app.use('/events', require('./events'));

	app.use('/profile', require('./profile'));

	app.use('/s3', require('./s3'));

}
