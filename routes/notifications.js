/*
* routes/events.js
* Router definition for the events endpoints
*/
var express = require('express');
var controller = require('../controllers/notifications');
//cors = require('cors');
const router = express.Router();

// middleware specific to this router
router.use(function timeLog(req, res, next) {
	'use strict';

  console.log('Events: Time: ', Date.now());
  next();
});

//var corsOptions = require('../lib/utils.js').corsOptions;

router.put('/pinned', controller.sendPinnedNotification);

module.exports = router;
