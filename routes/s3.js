/* routes/s3.router.js
 * Router defintion for /s3 endpoint
 *
 * */
const controller = require('../controllers/s3');

const express = require('express');

const router = express.Router();

router.post( '/getS3Url/', controller.getS3Url );

router.post( '/getS3Download/', controller.getS3DownloadUrl );

module.exports = router;
