/*
* routes/bridge.js
* Router definition for the bridges endpoints
*/
var express = require('express');
var controller = require('../controllers/bridges');
//cors = require('cors');
const router = express.Router();

// middleware specific to this router
router.use(function timeLog(req, res, next) {
	'use strict';

  console.log('Time: ', Date.now());
  next();
});

//var corsOptions = require('../lib/utils.js').corsOptions;

router.post('/invites/', controller.sendInvites);

module.exports = router;
