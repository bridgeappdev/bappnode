/* Bridge Node Server
* server.js
*
* Entry point for the Bridge App API server*/
'use strict'

const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const cors = require('cors');

const Logger = require('./lib/logger');

global.LOGGER = new Logger( process.env.NODE_ENV );

var firebase = require("firebase");
firebase.initializeApp({
  serviceAccount: __dirname + "/config/br1dg3co-91657dcc6f7e.json",
  databaseURL: "https://br1dg3co.firebaseio.com"
});

app.use(cors({credentials: false}));
//app.options('*', cors({credentials: false}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true }));

//Register routes
require('./routes')(app);

app.set('port', (process.env.PORT || 8100));
const server = app.listen(app.get('port'),function(){
	console.log('bappnode listening on port ' + server.address().port);
});
