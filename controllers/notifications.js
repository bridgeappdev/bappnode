/* /controller/profile.js
* Router definition for the /profile endpoints
*
*/

'use strict';

const firebase = require('firebase');

const jsonSchema = require('validation/jsonSchema');

const notifications = require('notifications');

const _buildPinNotification = function( requestData ){
	return {
		"type": "PIN",
		"read": false,
		"seen": false,
		"createdAt": firebase.database.ServerValue.TIMESTAMP,
		"meta": {
			"bridgeId"	:requestData.bridgeId,
			"whoId"		: requestData.whoId,
			"who"		: requestData.who,
			"pinnedId"	: requestData.pinnedId
		}
	};
}

const _sendPinnedNotification = function( req, res ) {

	const pinNotificationSchema =
		require('validation/schemas/notification.pin.schema');
	const data = req.body;

	try {
		jsonSchema.validateSchema(data, pinNotificationSchema);
	} catch (e) {
		e.errorDetail = jsonSchema.parseErrorArray(e.errorDetail);
		return res.status( 400 ).send( { "status": 400, "data":{ "error": e } } );
	}

	const fb = firebase.database();
	const bridgeMembersRef = fb.ref('bridges/' + req.body.bridgeId + '/members' );

	bridgeMembersRef.once( 'value', function( dataSnapshot ){

		if( dataSnapshot.exists() ){
			let memberIds = Object.keys( dataSnapshot.val() );
			//Test
			//let memberIds = [ "K3D4L7EcDEUjLM7HCfs" ];
			notifications.saveNotification(
				memberIds,
				_buildPinNotification( req.body ),
				function( err ){
					//Not sure we care...
				} );
			return res.status( 204 ).send();
		}else{
			return res.status( 404 ).send(
				{ status:404,
					message: "No members found for given bridgeId "
				}
			);
		}
	}, function( err ){
		return res.status(500).send(err);
	})
};

module.exports = {

	sendPinnedNotification: _sendPinnedNotification

};
