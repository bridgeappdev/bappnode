/* controllers/bridges.js
 *
 * Controller functions for the /bridges paths
 *
 * */
const sendgrid  = require('sendgrid')('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');
const swig      = require('swig');
const path      = require('path');

var bridgeController = module.exports = {};

bridgeController.sendInvites = function sendInvites(req, res){
	'use strict';

	let emailAddresses = req.body.emails;
	let locals = {
		userData: req.body.user,
		bridgeData: req.body.bridge
	};
	let body = swig.renderFile(path.resolve(__dirname, '../views/emails/sendInvites.html'), locals);
	sendgrid.send({
		to:       emailAddresses,
		from:     'info@chakachat.com',
		subject:  'Sent from Chaka',
		text:     'This an invite to join us on Chaka!',
		html:     body
	}, function(err, json) {
		if (err) { return res.status(500).send(err); }
		return res.send(json);
	});

};
