/*	MediaBucketManager.js
*
*/
'use strict';
//const S3_KEY = process.env.S3_KEY;
//const S3_SECRET = process.env.S3_SECRET;
const S3_KEY = "AKIAI36AQUJETXI2WBGQ";
const S3_SECRET = "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";

const AWS = require('aws-sdk');
AWS.config.update({
	accessKeyId: S3_KEY,
	secretAccessKey: S3_SECRET,
	region: 'us-east-1'
});
this._s3 = new AWS.S3();

var MediaBucketManager = module.exports;

MediaBucketManager.getPutSignedUrl = function(
	bucket,
	key,
	expires,
	headers,
	next
) {
	var _self = this;
	var params = {
		Bucket: bucket,
		Key: key,
		Expires: expires
	};
	for (var attrname in headers) {
		params[attrname] = headers[attrname];
	}
	_self._s3.getSignedUrl('putObject', params, function(error, data) {
		if (error) {
			// TODO: build contextual error
			if (error.code == "UnexpectedParameter" && error.message.search("ContentLength") > -1) {
				if (params.ContentLength) delete params.ContentLength;
				MediaBucketManager.getPutSignedUrl(
					bucket,
					key,
					expires,
					params,
					function(error, data) {
						if (error) {
							next(error);
						} else {
							return next(null, data);
						}
					}
				);
			} else {
				return next(error);
			}
		} else {
			return next(null, data);
		}
	});
};

MediaBucketManager.getGetSignedUrl = function(
	bucket,
	key,
	expires,
	next
) {
	var _self = this;
	var params = {
		Bucket: bucket,
		Key: key,
		Expires: expires
	};

	_self._s3.getSignedUrl('getObject', params, function(error, data) {
		if (error) {
			return next( error );
		} else {
			return next( null, data );
		}
	});
};
