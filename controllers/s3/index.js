/* /controller/s3/index.js
* Router definition for the /s3 endpoints
*
*/

'use strict';

//const S3_BUCKET = process.env.S3_BUCKET;
const S3_BUCKET = 'files.chakachat.com';

const s3Manager = require('./s3Manager');

const _getS3Url = function( req, res ) {

	//const bridgeId = req.body.bridgeid;
	//delete req.body.bridgeid;
	const username = req.body.username;
	const filename = req.body.filename;
	delete req.body.username;
	delete req.body.filename;

	const new_filename = filename.substring(0, filename.lastIndexOf("."))
	 	.replace(/[^a-zA-Z0-9-_]/g, '') + "_" + Date.now()
		+ filename.substring(filename.lastIndexOf("."));

	const bucket = S3_BUCKET;
	//const key = userId + "/" + bridgeId + "/" + new_filename;
	const key = username + "/" + new_filename;
	//const key = new_filename;
	const expires = 120;
	const headers = req.body;
	s3Manager.getPutSignedUrl(
		bucket,
		key,
		expires,
		headers,
		function(error, s3URL) {
			if (error) {
				const err = new ERROR(
					'Error in S3Manager:: ' + error.message
				);
				return res.status(500).send( err );
			} else {
				return res.send( {'s3url': s3URL, 'key': key} );
			}
		}
	);
};

const _getS3DownloadUrl = function( req, res ) {

	const bucket = S3_BUCKET;
	const key = req.body.key;
	const expires = 120;
	s3Manager.getGetSignedUrl(
		bucket,
		key,
		expires,
		function(error, s3URL) {
			if (error) {
				const err = new ERROR(
					'Error in S3Manager:: ' + error.message
				);
				return res.status(500).send( err );
			} else {
				return res.send( {'s3Downloadurl': s3URL} );
			}
		}
	);
};


module.exports = {

	getS3Url : _getS3Url,

	getS3DownloadUrl : _getS3DownloadUrl

};
