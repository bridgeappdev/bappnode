/* /controller/profile.js
* Router definition for the /profile endpoints
*
*/

'use strict';

const Firebase = require('firebase');

const _checkUsername = function( req, res ) {

	const candidateName = req.body.candidate;
	const fb = new Firebase('https://br1dg3co.firebaseio.com/');
	const usernameRef = fb.child('usernames');

	usernameRef.orderByKey()
	.startAt( candidateName )
	.endAt( candidateName+"\uf8ff" )
	.once( 'value', function( dataSnapshot ){

		if( !dataSnapshot.exists() ){
			return res.send( { result:true } );
		}else{
			return res.send( { result:false } );
		}
	}, function( err ){
		return res.status(500).send(err);
	})
};

const _addUsername = function( req, res ) {

	const username = req.body.username;
	const fb = new Firebase('https://br1dg3co.firebaseio.com/');
	const usernameRef = fb.child('usernames/' + username );

	usernameRef.transaction( function(){
		return true;
	}, function( error, committed, snapshot ){
		if( error ){
			return res.status(500).send(err);
		}
		if( committed ){
			return res.send();
		}else{
			return res.status(409).send();
		}
	});
};

module.exports = {

	checkUsername : _checkUsername,

	addUsername : _addUsername

};
